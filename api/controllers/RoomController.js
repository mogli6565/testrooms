/**
 * RoomController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
    index:function(req,res){
        var s=req.param('id');
        res.locals.almog = "sup";
        res.view('room/index',{id:s}); // this work too: res.view()
    },

    rooms:function(req,res){
        Room.find()
            .where({ id: { '>': 1 }})
            .limit(100)
            .sort('id')
            .done(function(err, rooms) {
                var s = "aaa";
                res.json({
                    rooms:rooms
                });
            });
    },

    rooms2:function(req,res){
    var roomId = req.param('roomId');
        res.json({
            roomId:roomId
        })
    },



    create:function(req,res){
        var roomName = req.param("name");
        Room.create({name:roomName}).done(function(err,room){
            Room.find()
                .where({ id: { '>': 1 }})
                .limit(100)
                .sort('id')
                .done(function(err, rooms) {
//                    res.json({
//                        rooms:rooms
//                    });
                    sails.io.sockets.emit('UpdateRooms',rooms);
                });
        });


    },

    join:function(req,res){
        var roomId = req.param("roomId");
        Room.findOne({id:roomId}).done(function(err,room)
        {
            if(room){

                Message.find({ roomId: roomId})
                    .limit(100)
                    .done(function(err, messages) {
                        req.socket.join(roomId);
                        room.messages = messages;
                        sails.io.sockets.in(room.roomId).emit('successfullyJoinedRoom',room);
                        res.json({
                            success:true,
                            room:room
                        })
                    });

            }else{
                res.json({
                    success:false

                })
            }

        });
    },

    leave:function(req,res){
        var room = req.param("room");
        req.socket.leave(room.id);
        req.session.roomId = null;
        res.json({
            success:true
        })

    },

    message:function(req,res){
        var message = req.param("message");
        var roomId = req.param("roomId");

        Message.create({roomId:roomId, content:message}).done(function(err,message){
            sails.io.sockets.in(roomId).emit('newMessage',message);
        });
    },



    /**
     * Overrides for the settings in `config/controllers.js`
     * (specific to RoomController)
     */
    _config: {}


};
