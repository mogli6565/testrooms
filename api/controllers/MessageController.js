/**
 * MessageController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {

    create: function (req,res)
    {
        var socket = req.socket;
        var io = sails.io;


        var runningCode;
        Code.findOne({id:1}).done(function(err,code)
            {
                console.log(code);
                console.log(code.runningCode);
                runningCode = parseInt(code.runningCode) + 1;
                code.runningCode= runningCode;
                code.save(function(err){});
            });


        Message.create({roomName:runningCode}).done(function(err,message){
                        //     Message.publishCreate({id:message.id,buttonPressed:message.buttonPressed});
                        socket.join(message.roomName)
                        req.session.myroom = message.roomName;

        });

//        res.view('home/join');
        io.sockets.in(req.session.myroom).emit('showIt',req.session.myroom);
    },


    join: function (req,res)
    {
        var socket = req.socket;
        var io = sails.io;
        res.view('home/index');
        Message.findOne({roomName:req.body.roomWanted}).done(function(err,message)
        {
            if(err){return console.log(err);}
            if(!message){return console.log("not found");}
            else
            {
               var room = message.roomName;
                socket.join(room);
                io.sockets.in(room).emit('newUser','new user just logged in to room:' + room);
            }
        });


    },







  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to MessageController)
   */
  _config: {}

  
};
