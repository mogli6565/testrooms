/**
 * Question
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {

      type:"string",
      text:"string",
      answer:"string",
      imageUrl:"string"

  	/* e.g.
  	nickname: 'string'
  	*/
    
  }

};
