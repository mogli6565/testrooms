/**
 * Game
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {

      name:"string",
      code:"string",
      questions:"string" /*1,5,4,7,12,4,6*/
  	/* e.g.
  	nickname: 'string'
  	*/
    
  }

};
