/**
 * app.js
 *
 * This file contains some conventional defaults for working with Socket.io + Sails.
 * It is designed to get you up and running fast, but is by no means anything special.
 *
 * Feel free to change none, some, or ALL of this file to fit your needs!
 */


(function (io) {

    // as soon as this file is loaded, connect automatically,
    var socket = io.connect();
    if (typeof console !== 'undefined') {
        log('Connecting to Sails.js...');
    }

    socket.on('connect', function socketConnected() {

        // Listen for Comet messages from Sails
        socket.on('message', function messageReceived(message) {

            ///////////////////////////////////////////////////////////
            // Replace the following with your own custom logic
            // to run when a new message arrives from the Sails.js
            // server.
            ///////////////////////////////////////////////////////////
            log('New comet message received :: ', message);
            //////////////////////////////////////////////////////

        });


        ///////////////////////////////////////////////////////////
        // Here's where you'll want to add any custom logic for
        // when the browser establishes its socket connection to
        // the Sails.js server.
        ///////////////////////////////////////////////////////////
        log(
                'Socket is now connected and globally accessible as `socket`.\n' +
                'e.g. to send a GET request to Sails, try \n' +
                '`socket.get("/", function (response) ' +
                '{ console.log(response); })`'
        );
        ///////////////////////////////////////////////////////////


    });


    // Expose connected `socket` instance globally so that it's easy
    // to experiment with from the browser console while prototyping.
    window.socket = socket;


    // Simple log function to keep the example simple
    function log () {
        if (typeof console !== 'undefined') {
            console.log.apply(console, arguments);
        }
    }


})(

    // In case you're wrapping socket.io to prevent pollution of the global namespace,
    // you can replace `window.io` with your own `io` here:
    window.io

);


(function ($) {
    /*
     socket.request('/message');

     $("#Bcreate").click(function () {
     socket.get("/message/create", {
     },function(response){});
     });

     $("#mtoroom").click(function () {
     var roomWanted = $("#roomn").val();
     socket.post("/message/join", {
     roomWanted: roomWanted
     });
     });
$.get("/aaaa",{},function(data){
    ...
});

     socket.on('showIt', function (msg) {
     $("#answer").html("hello! welcome to room " + msg);
     });

     socket.on('newUser', function (msg) {
     $("#answer").append('</br>' + msg);
     });*/



    /************************************************/
    /************************************************/
    /************************************************/
    var myRoom;
    function setAllRooms(rooms){
        var ul = $("#availableRoomsDiv ul");
        $(ul).html("");
        for (var i=0;i<rooms.length; i++){
            $(ul).append("<li><a href='#' class='roomLink' id='"+ rooms[i].id + "'> Room Name: " + rooms[i].name +  ", Id: " + rooms[i].id + "</a></li>");
            //console.log($(ul).html());
        }
        $(ul).slideDown('slow');
    }

    /**
     * When page is being loaded
     */
    socket.get("/room/rooms", {
    },function(response){
        setAllRooms(response.rooms);
    });

    socket.get("/room/rooms2", {roomId:myRoom.id
    },function(response){
        $('#roomDivPage').append(response.roomId);
    });


    /**
     * When create new room is being clicked
     */
    $("#createRoom").click(function () {
        var roomName =  $("#roomNameInput").val();
        var data = {name : roomName};
        socket.post("/room/create",data,
            function(response){

            })
    });

    /**
     * When user joining a room
     */

        $("#availableRoomsDiv ul").on('click', '.roomLink', function(){
        //alert('click!!');
        var roomId =  this.id;
        var data = {roomId : roomId};
        socket.post("/room/join",data,
            function(response){
                if(response.success){
                    $("#roomDiv").fadeIn();

                    for(var i=0;i<response.room.messages.length; i++){
                        addMessage(response.room.messages[i].content);
                    }
                    $("#roomDiv strong#roomNameStrong").html(response.room.name);
                    myRoom = response.room;

                }else{
                    alert("room does not exists");
                }
            })
    });



    socket.on('successfullyJoinedRoom', function (room) {
        console.log("new Group member joined to: " + room.name);
    });


    $("#leaveRoomButton").click(function () {
        socket.post("/room/leave",{room: myRoom},
            function(response){
                if(response.success == true){
                    $("#roomDiv").fadeOut();
                    $("#roomDiv ul").html("");
                }else{
                    alert("something went wrong");
                }

            })
    });

    function addMessage(message){
        $("#roomDiv ul").append("<li>" + message + "</li>");
    }


    $("#sendMessage").click(function () {
        var message = $("#messageInput").val();
        socket.post("/room/message",{roomId: myRoom.id, message:message},
            function(response){

            })
    });

    socket.on('UpdateRooms', function (rooms) {
        setAllRooms(rooms);
    });

    socket.on('newMessage', function (message) {
        addMessage(message.content);
    });



})(jQuery);
